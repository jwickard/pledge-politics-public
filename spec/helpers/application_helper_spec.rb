require 'spec_helper'

# Specs in this file have access to a helper object that includes

describe ApplicationHelper do
  describe 'is_referral?' do
    describe 'with pledge token' do
      it 'should return false if pledge.pledge_token key is nil' do
        helper.is_referral?.should be_false
      end

      it 'should return false if pledge.pledge_token is set but it is empty' do
        session['pledge.pledge_token'] = ''

        helper.is_referral?.should be_false
      end

      it 'should return true if pledge.pledge_token is set to a value' do
        session['pledge.pledge_token'] = Devise.friendly_token

        helper.is_referral?.should be_true
      end
    end

    describe 'with invite token' do
      it 'should return false if pledge.invite_token key is nil' do
        helper.is_referral?.should be_false
      end

      it 'should return false if pledge.invite_token is set but it is empty' do
        session['pledge.invite_token'] = ''

        helper.is_referral?.should be_false
      end

      it 'should return true if pledge.invite_token is set to a value' do
        session['pledge.invite_token'] = Devise.friendly_token

        helper.is_referral?.should be_true
      end
    end
  end

  describe 'tweet_text' do
    it 'should URI encode our canned tweet template' do
      candidate = Candidate.make!
      helper.tweet_text(candidate).should eql(URI.escape("Check Out My Pledge To Vote For #{candidate.name}:", Regexp.new("[^#{URI::PATTERN::UNRESERVED}]")))
    end
  end
end
