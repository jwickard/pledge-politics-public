require 'spec_helper'

describe OmniauthCallbacksController do

  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
    session["user_return_to"] = pledge_new_path('ronpaul')
  end

  describe 'facebook' do
    describe 'existing user' do
      it 'should sign in user' do
        user = User.make!(:facebook)

        User.should_receive(:find_for_facebook_oauth).with(OmniAuth.config.mock_auth[:facebook], nil).and_return(user)

        get 'facebook'

        warden.authenticated?(:user).should be_true
      end

      it 'should redirect to pledge page' do
        user = User.make!(:facebook)

        User.should_receive(:find_for_facebook_oauth).with(OmniAuth.config.mock_auth[:facebook], nil).and_return(user)

        get 'facebook'

        response.should redirect_to(pledge_new_path('ronpaul'))
      end
    end

    describe 'with non-existing user' do
      it 'create a new user with passed data' do
        get 'facebook'

        #session['devise.facebook_data'].should eql(OmniAuth.config.mock_auth[:facebook])
        User.last.email.should eql(@request.env['omniauth.auth']['info']['email'])
      end

      it 'should redirect user to registration page' do
        get 'facebook'

        response.should redirect_to(pledge_new_path('ronpaul'))
      end
    end
  end

  describe 'twitter' do
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:twitter]
    end

    describe 'existing user' do
      it 'should sign in user' do
        user = User.make!(:twitter)

        User.should_receive(:find_for_twitter_oauth).with(OmniAuth.config.mock_auth[:twitter], nil).and_return(user)

        get 'twitter'

        warden.authenticated?(:user).should be_true
      end

      it 'should redirect to pledge page' do
        user = User.make!(:twitter)

        User.should_receive(:find_for_twitter_oauth).with(OmniAuth.config.mock_auth[:twitter], nil).and_return(user)

        get 'twitter'

        response.should redirect_to(pledge_new_path('ronpaul'))
      end
    end

    describe 'with non-existing user' do
      it 'create a new user with passed data' do
        get 'twitter'

        User.last.user_name.should eql(@request.env['omniauth.auth']['info']['nickname'])
      end

      it 'should redirect user to registration page' do
        get 'twitter'

        response.should redirect_to(pledge_new_path('ronpaul'))
      end
    end
  end

  describe 'google' do
      before(:each) do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        @request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:google]
      end

      describe 'existing user' do
        it 'should sign in user' do
          user = User.make!(:google)

          User.should_receive(:find_for_open_id_oauth).with(OmniAuth.config.mock_auth[:google], nil).and_return(user)

          get :google

          warden.authenticated?(:user).should be_true
        end

        it 'should redirect to pledge page' do
          user = User.make!(:google)

          User.should_receive(:find_for_open_id_oauth).with(OmniAuth.config.mock_auth[:google], nil).and_return(user)

          get :google

          response.should redirect_to(pledge_new_path('ronpaul'))
        end
      end

      describe 'with non-existing user' do
        it 'create a new user with passed data' do
          get :google

          User.last.user_name.should eql(@request.env['omniauth.auth']['info']['email'])
          User.last.email.should eql(@request.env['omniauth.auth']['info']['email'])
        end

        it 'should redirect user to registration page' do
          get :google

          response.should redirect_to(pledge_new_path('ronpaul'))
        end
      end
    end
end
