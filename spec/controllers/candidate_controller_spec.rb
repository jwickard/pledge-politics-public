require 'spec_helper'

describe CandidateController do

  describe "GET 'show'" do
    it "finds a candidate by slug and puts it into scope" do
      candidate = Candidate.make!

      get :show, slug: candidate.slug

      assigns(:candidate).should eql(candidate)

      response.should render_template :show
    end

    it 'should set user_return_to url in session' do
      candidate = Candidate.make!

      get :show, slug: candidate.slug

      session['user_return_to'].should eql(pledge_new_path(candidate.slug))
    end

    it "should assign a total pledge count" do
      candidate = Candidate.make!

      get :show, slug: candidate.slug

      #TODO need to add pledges?
      assigns[:total_pledge_count].should eql(0)
    end

    it "should assign a total accepted pledge count" do
      candidate = Candidate.make!

      get :show, slug: candidate.slug

      assigns[:total_accepted].should eql(0)
    end

    it "should redirect to root if a candidate is not found" do
      get :show, slug: "noone"

      response.should redirect_to root_path
    end

    it "should redirect to root if a candidate is not enabled" do
      candidate = Candidate.make!(active: false)

      get :show, slug: candidate.slug

      response.should redirect_to root_path
    end
  end
end
