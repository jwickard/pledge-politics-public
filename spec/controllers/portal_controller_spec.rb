require 'spec_helper'

describe PortalController do

  describe "GET 'index'" do
    it "returns http success" do
      get :index
      response.should be_success
    end

    it "assigns current active candidates" do
      one = Candidate.make!
      two = Candidate.make!

      get :index

      assigns(:candidates).should eql([one, two])
    end
  end

end
