require 'spec_helper'

describe PledgeController do

  describe "GET 'new'" do
    it "should redirect to root path if you are not authenticated" do
      get :new

      response.should_not be_success
      response.should redirect_to(new_user_session_path)
    end

    it "should put the session candidate into page scope" do
      candidate = Candidate.make!
      user = User.make!
      sign_in user

      get :new, slug: candidate.slug

      assigns(:candidate).should eql(candidate)
    end

    it 'should render new if you are authenticated' do
      user = User.make!
      sign_in user

      get :new

      response.should render_template :new
    end
  end

  describe "GET 'create'" do
    login_user

    it 'returns http success' do
      get :create

      response.should be_success
    end

    describe 'with json format' do
      it 'creates a pledge associated with logged in user' do
        get :create, format: :json

        json_response = JSON.parse(response.body)

        json_response['user_id'].should eq(@current_user.id)
      end

      it 'populates a token on the new pledge' do
        get :create, format: :json

        json_response = JSON.parse(response.body)

        json_response['token'].should_not be_nil
      end

      it 'renders new pledge as json' do
        get :create, format: :json

        pledge_json = JSON.parse(Pledge.last.to_json)

        pledge_json.should == JSON.parse(response.body)
      end

      it 'renders json error if pledge is duplicate' do
        get :create, format: :json
        get :create, format: :json

        response.should_not be_success

        json_response = JSON.parse(response.body)

        json_response['user_id'].should == ["You have already pledged with this account!"]
      end
    end

    describe 'with existing invite' do
      it 'should associate this pledge with the existing invite' do
        invite = Invite.make!(:mailable)
        session['pledge.invite_token'] = invite.token

        get :create

        #get the new pledge
        pledge = Pledge.last

        #refresh invite
        invite = Invite.find(invite.id)

        invite.accepted_pledge.should eql(pledge)
      end

      it 'should update the existing invite to accepted' do
        invite = Invite.make!(:mailable)
        session['pledge.invite_token'] = invite.token

        get :create

        invite = Invite.find(invite.id)

        invite.accepted?.should be_true
      end

      it 'should associate the invite with the logged in user' do
        invite = Invite.make!(:mailable)
        session['pledge.invite_token'] = invite.token

        get :create

        invite = Invite.find(invite.id)

        invite.user.should eql(@current_user)
      end
    end

    describe 'with existing pledge' do
      it 'should create an invite to match the passed pledge' do
        pledge = Pledge.make!
        session['pledge.pledge_token'] = pledge.token

        get :create

        invite = Invite.last

        invite.invite_pledge_id.should eql(pledge.id)
      end

      it 'should default the invite to accepted' do
        pledge = Pledge.make!
        session['pledge.pledge_token'] = pledge.token

        get :create

        invite = Invite.last

        invite.accepted?.should be_true
      end

      it 'should default the invite user to the current user' do
        pledge = Pledge.make!
        session['pledge.pledge_token'] = pledge.token

        get :create

        invite = Invite.last

        invite.user.id.should eql(@current_user.id)
      end

      it 'with json format should associate our pledge as the accepted pledge' do
        pledge = Pledge.make!
        session['pledge.pledge_token'] = pledge.token

        get :create, format: :json

        invite = Invite.last

        response.body.should eql(invite.accepted_pledge.to_json)
      end
    end
  end

  describe "GET 'show'" do
    it 'should not require authentication' do
      pledge = Pledge.make!
      get :show, token: pledge.token
      response.should redirect_to view_candidate_path(pledge.pledge_topic.slug)
    end

    it 'should store pledge token in session' do
      pledge = Pledge.make!

      get :show, token: pledge.token

      session['pledge.pledge_token'].should eql(pledge.token)
    end
  end

end
