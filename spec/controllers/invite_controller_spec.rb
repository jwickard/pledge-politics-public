require 'spec_helper'

describe InviteController do

  describe "POST create" do
    login_user

    describe 'valid params' do
      it "returns http success" do
        pledge = Pledge.make!
        post :create, invite: { invite_pledge_id: pledge.id, email: 'random@user.com' }
        response.should be_success
      end

      it 'should create an invite associated with the pledge' do
        pledge = Pledge.make!
        post :create, invite: { invite_pledge_id: pledge.id, email: 'random@user.com' }

        Invite.where(invite_pledge_id: pledge.id).count.should eq(1)
      end

      it 'should set the email address we passed to it' do
        pledge = Pledge.make!
        post :create, invite: { invite_pledge_id: pledge.id, email: 'random@user.com' }

        invite = Invite.find_by_invite_pledge_id(pledge.id)

        invite.email.should eql('random@user.com')
      end

      it 'should create a token for the invite' do
        pledge = Pledge.make!
        post :create, invite: { invite_pledge_id: pledge.id, email: 'random@user.com' }

        invite = Invite.find_by_invite_pledge_id(pledge.id)

        invite.token.should_not be_nil
      end

      it 'should send the invitee and email' do
        #stub out our mail integration
        mail = mock('mail')
        mail.should_receive(:deliver)
        InviteMailer.should_receive(:invite_notification).and_return(mail)

        pledge = Pledge.make!

        post :create, invite: { invite_pledge_id: pledge.id, email: 'random@user.com' }

        response.should be_success
      end

      it 'should create an invite record for multiple addresses if passed a csv string of email addresses' do
        pledge = Pledge.make!

        post :create, invite: { invite_pledge_id: pledge.id, email: 'random@user.com, random1@user.com, random2@user.com' }

        Invite.where(invite_pledge_id: pledge.id).count.should eq(3)
      end

      it 'should send out an email to multiple invitees if passed a csv string of email addresses' do
        #stub our mailer
        mail = mock('mail')
        mail.should_receive(:deliver).exactly(3).times
        InviteMailer.should_receive(:invite_notification).exactly(3).times.and_return(mail)

        pledge = Pledge.make!

        post :create, invite: { invite_pledge_id: pledge.id, email: 'random@user.com, random1@user.com, random2@user.com' }

        response.should be_success
      end

      it 'should return an invite serialized as json if passed a single address' do
        pledge = Pledge.make!

        post :create, invite: { invite_pledge_id: pledge.id, email: 'random@user.com' }

        invite = Invite.last

        json_response = JSON.parse(response.body)[0]  #we are going to get an array, we only want the first element
        #I like to validate the two parameters we passed independently
        json_response['invite_pledge_id'].should eql(pledge.id)
        json_response['email'].should eql('random@user.com')

        #compare the whole serialized record
        JSON.parse(invite.to_json).should == json_response
      end

      it 'should return an invite list serialized as json if passed multiple addresses' do
        pledge = Pledge.make!

        post :create, invite: { invite_pledge_id: pledge.id, email: 'random@user.com, random1@user.com, random2@user.com' }

        invites = Invite.where(invite_pledge_id: pledge.id).all

        invites.size.should eq(3)

        invites.each do |invite|
          response.body.should include_json(invite.to_json)
        end
      end

      describe 'list with bad entries' do
        it 'should skip creating a record if any entries are bad' do
          pledge = Pledge.make!

          post :create, invite: { invite_pledge_id: pledge.id, email: 'random@user.com, random1@user.com,,, random2@user.com' }

          Invite.where(invite_pledge_id: pledge.id).count.should eq(3)
        end
      end
    end

    describe 'with invalid parameters' do
      it 'should render json error for required invite_pledge_id' do
        post :create, invite: { email: 'random@user.com' }

        response.should_not be_success
        json = JSON.parse(response.body)
        json['invite_pledge_id'][0].should == "can't be blank"
      end

      it 'should render json error for required email' do
        pledge = Pledge.make!

        post :create, invite: { pledge_id: pledge.id }

        response.should_not be_success
        json = JSON.parse(response.body)
        json['email'][0].should == "can't be blank"
      end
    end
  end

  describe "GET 'show'" do
    it 'should not require authentication' do
      invite = Invite.make!(:mailable)

      get :show, token: invite.token

      response.should redirect_to(view_candidate_path(invite.inviting_pledge.pledge_topic.slug))
    end

    it 'should put invite token into session scope' do
      invite = Invite.make!(:mailable)

      get :show, token: invite.token

      session['pledge.invite_token'].should eql(invite.token)
    end
  end

end
