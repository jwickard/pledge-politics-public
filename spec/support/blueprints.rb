require 'machinist/active_record'

Candidate.blueprint do
  name { "Candidate #{sn}" }
  slug { "can#{sn}" }
  hash_tag { "#can#{sn}" }
  active { true }
end

User.blueprint do
  user_name { "user#{sn}" }
  provider { "pledgepolitics" }
  email { "user#{sn}@example.com"}
  password { "password" }
end

User.blueprint(:facebook) do
  user_name { "user#{sn}" }
  provider { "facebook" }
  email { "user#{sn}@example.com" }
  password { "password" }
end

User.blueprint(:twitter) do
  user_name { "user#{sn}" }
  provider { "twitter" }
  password { "password" }
end

User.blueprint(:google) do
  user_name { "user#{sn}@gmail.com" }
  email { "user#{sn}@gmail.com" }
  password { "password" }
  provider { "google" }
end

Pledge.blueprint do
  user
  token { Devise.friendly_token }
  pledge_topic { Candidate.make! }
end

Invite.blueprint(:mailable) do
  inviting_pledge { Pledge.make!(user: User.make!) }
  email { "pledge_recipient#{sn}@example.com" }
  token { Devise.friendly_token }
end

Invite.blueprint(:accepted) do
  inviting_pledge { Pledge.make!(user: User.make!) }
  accepted_pledge { Pledge.make!(user: User.make!, pledge_topic: object.inviting_pledge.pledge_topic ) }
  accepted { true }
  user { User.make! }
  email { "pledge_recipient#{sn}@example.com" }
  token { Devise.friendly_token }
end

# Add your blueprints here.
#
# e.g.
#   Post.blueprint do
#     title { "Post #{sn}" }
#     body  { "Lorem ipsum..." }
#   end
