module ControllerMacros
  def login_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      user = User.make!
      sign_in user
      @current_user = user
    end
  end
end