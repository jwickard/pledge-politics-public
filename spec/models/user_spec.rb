require 'spec_helper'

describe User do
  def facebook_oauth
    OmniAuth.config.mock_auth[:facebook]
  end

  def twitter_oauth
    OmniAuth.config.mock_auth[:twitter]
  end

  def google_oauth
    OmniAuth.config.mock_auth[:google]
  end

  def create_facebook_authentication(user)
    user.authentications.create!({ uid: facebook_oauth['uid'], provider: facebook_oauth['provider'], token: 'tokenshouldchange' })
  end

  def create_twitter_authentication(user)
    #TODO add the secret to this test class.
    user.authentications.create!({ uid: twitter_oauth['uid'], provider: twitter_oauth['provider'], token: 'tokenshouldchange' })
  end

  def create_google_authentication(user)
    user.authentications.create!({ uid: google_oauth['uid'], provider: google_oauth['provider'] })
  end

  it 'should return false from email_required?' do
    user = User.new

    user.email_required?.should be_false
  end

  describe 'new_with_session' do
    it 'should default the provider to pledgeforpaul' do
      user = User.new_with_session(Hash['email', 'joeuser@example.com', 'password', 'password', 'password_confirmation', 'password'], {})

      user.provider.should eql('pledgeforpaul')
    end

    it 'should default the username to email' do
      user = User.new_with_session(Hash['email', 'joeuser@example.com', 'password', 'password', 'password_confirmation', 'password'], {})

      user.user_name.should eql(user.email)
    end
  end

  describe 'find_for_facebook_oauth' do

    describe 'without signed in user' do
      it 'should create a new user and authentication if one does not exist' do
        user = User.find_for_facebook_oauth(facebook_oauth)
        user.should_not be_nil
        user.email.should eql(facebook_oauth['info']['email'])
        user.user_name.should eql(facebook_oauth['info']['nickname'])
        user.authentications.size.should eql(1)

        auth = user.authentications.first

        auth.token_expires.should be_true
        auth.token_duration.should eql(facebook_oauth['credentials']['expires_at'].to_i)
        auth.token.should eql(facebook_oauth['credentials']['token'])
        auth.provider.should eql(facebook_oauth['provider'])
        auth.uid.should eql(facebook_oauth['uid'])
      end

      it 'should not return a readonly record' do
        User.make!({:email => 'user@example.com'})

        found = User.find_for_facebook_oauth(facebook_oauth)

        found.readonly?.should be_false
      end

      #TODO this matches based on that email and not an actual authentication.
      it 'should return existing user for matching authentication' do
        user = User.make!({:email => 'user@example.com'})
        create_facebook_authentication(user)

        found = User.find_for_facebook_oauth(facebook_oauth)

        found.should eql(user)
      end

      it 'should update token when matching authentication exists' do
        user = User.make!({:email => 'user@example.com'})
        create_facebook_authentication(user)

        user = User.find_for_facebook_oauth(facebook_oauth)

        authentication = user.authentications.first

        authentication.token.should eql(facebook_oauth['credentials']['token'])
      end

      it 'should find a verified user by email if no authentication exists' do
        user = User.make!({:email => 'user@example.com'})

        found = User.find_for_facebook_oauth(facebook_oauth)

        found.should eql(user)
      end

      it 'should find a user only by authentication if verified but email does not match' do
        user = User.make!({:email => 'nonmatching@email.com'})
        create_facebook_authentication(user)

        found = User.find_for_facebook_oauth(facebook_oauth)

        found.should eql(user)
      end
    end

    describe 'with signed in user' do

      it 'should add a new authentication if one does not exist' do
        user = User.make!({:email => 'user@example.com'})

        #we don't have any authentications
        user.authentications.empty?.should be_true

        user = User.find_for_facebook_oauth(facebook_oauth, user)

        authentication = user.authentications.first

        authentication.uid.should eql(facebook_oauth['uid'])
        authentication.provider.should eql(facebook_oauth['provider'])
        authentication.token.should eql(facebook_oauth['credentials']['token'])

        authentication.persisted?.should be_true
      end

      it 'should update the token if authentication exists' do
        user = User.make!({:email => 'user@example.com'})
        create_facebook_authentication(user)

        user = User.find_for_facebook_oauth(facebook_oauth, user)

        authentication = user.authentications.first

        authentication.token.should eql(facebook_oauth['credentials']['token'])
      end
    end
  end

  describe 'find_for_open_id_oauth' do

      describe 'without signed in user' do
        it 'should create a new user and authentication if one does not exist' do
          user = User.find_for_open_id_oauth(google_oauth)
          user.should_not be_nil
          user.email.should eql(google_oauth['info']['email'])
          user.user_name.should eql(google_oauth['info']['email'])
          user.authentications.size.should eql(1)

          auth = user.authentications.first

          auth.token_expires.should be_false
          auth.token_duration.should be_nil
          auth.token.should be_nil
          auth.provider.should eql(google_oauth['provider'])
          auth.uid.should eql(google_oauth['uid'])
        end

        it 'should not build a duplicate authentication for an existing matching user' do
          user = User.make!(email: google_oauth['info']['email'])
          create_google_authentication(user)

          found = User.find_for_open_id_oauth(google_oauth)

          found.should eql(user)
          found.authentications.size.should eql(1)
        end

        it 'should not return a readonly record' do
          User.make!({:email => 'joegmail@gmail.com'})

          found = User.find_for_open_id_oauth(google_oauth)

          found.readonly?.should be_false
        end

        it 'should return existing user for matching authentication' do
          user = User.make!({:email => 'user@notmatchingemail.com'})
          create_google_authentication(user)

          found = User.find_for_open_id_oauth(google_oauth)

          found.should eql(user)
          found.email.should eql(user.email)
        end

        it 'should find a verified user by email if no authentication exists' do
          user = User.make!({:email => 'joegmail@gmail.com'})

          found = User.find_for_open_id_oauth(google_oauth)

          found.should eql(user)
        end
      end

      describe 'with signed in user' do
        it 'should add a new authentication if one does not exist' do
          user = User.make!({:email => 'user@example.com'})

          #we don't have any authentications
          user.authentications.empty?.should be_true

          found = User.find_for_open_id_oauth(google_oauth, user)

          found.should eql(user)

          authentication = found.authentications.first

          authentication.uid.should eql(google_oauth['uid'])
          authentication.provider.should eql(google_oauth['provider'])

          authentication.persisted?.should be_true
        end
      end
    end

  describe 'find_for_twitter_oauth' do

      describe 'without signed in user' do
        it 'should create a new user and authentication if one does not exist' do
          user = User.find_for_twitter_oauth(twitter_oauth)
          user.should_not be_nil
          user.email.should be_nil
          user.user_name.should eql(twitter_oauth['info']['nickname'])
          user.authentications.size.should eql(1)

          auth = user.authentications.first

          auth.token_expires.should be_nil
          auth.token_duration.should be_nil
          auth.token.should eql(twitter_oauth['credentials']['token'])
          #TODO add twitter secret to authentication object and this test
          auth.secret.should eql(twitter_oauth['credentials']['secret'])
          auth.provider.should eql(twitter_oauth['provider'])
          auth.uid.should eql(twitter_oauth['uid'])
        end

        #TODO this needs to be rewritten, it will always return a new record. add some tests that deal with the authentication lookup vs. username/provider lookup.
        it 'should not return a readonly record' do
          User.make!(:twitter)

          found = User.find_for_twitter_oauth(twitter_oauth)

          found.readonly?.should be_false
        end

        it 'should return existing user for matching authentication' do
          user = User.make!(:twitter)
          create_twitter_authentication(user)

          found = User.find_for_twitter_oauth(twitter_oauth)

          found.should eql(user)
        end

        it 'should update token and secret when matching authentication exists' do
          user = User.make!(:twitter)
          create_twitter_authentication(user)

          user = User.find_for_twitter_oauth(twitter_oauth)

          authentication = user.authentications.first

          authentication.token.should eql(twitter_oauth['credentials']['token'])
          authentication.secret.should eql(twitter_oauth['credentials']['secret'])
        end

        it 'should find an existing user by user_name and provider without existing authentication' do
          user = User.make!({ email: nil, provider: twitter_oauth['provider'], user_name: twitter_oauth['info']['nickname']})

          found = User.find_for_twitter_oauth(twitter_oauth)

          found.should eql(user)
        end

        it 'should find an existing user by existing authentication' do
          user = User.make!(:twitter)
          create_twitter_authentication(user)

          found = User.find_for_twitter_oauth(twitter_oauth)

          found.should eql(user)
        end
      end

      describe 'with signed in user' do

        it 'should add a new authentication if one does not exist' do
          user = User.make!(:twitter)

          #we don't have any authentications
          user.authentications.empty?.should be_true

          user = User.find_for_twitter_oauth(twitter_oauth, user)

          authentication = user.authentications.first

          authentication.token_expires.should be_nil
          authentication.token_duration.should be_nil
          authentication.uid.should eql(twitter_oauth['uid'])
          authentication.provider.should eql(twitter_oauth['provider'])
          authentication.token.should eql(twitter_oauth['credentials']['token'])
          authentication.secret.should eql(twitter_oauth['credentials']['secret'])
          authentication.persisted?.should be_true
        end

        it 'should update the token if authentication exists' do
          user = User.make!(:twitter)
          create_twitter_authentication(user)

          user = User.find_for_twitter_oauth(twitter_oauth, user)

          authentication = user.authentications.first

          authentication.token.should eql(twitter_oauth['credentials']['token'])
          authentication.secret.should eql(twitter_oauth['credentials']['secret'])
        end
      end
    end
end
