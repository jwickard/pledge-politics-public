require 'spec_helper'

describe PledgeTopic do
  it "should return a count of it's total pledges" do
    candidate = Candidate.make!

    Pledge.make!(pledge_topic: candidate)
    Pledge.make!(pledge_topic: candidate)
    Pledge.make!(pledge_topic: candidate)

    candidate.pledge_count.should eql(3)
  end

  it 'should return the total count of accepted invites for a specific candidate' do
    invite_one = Invite.make!(:accepted)
    invite_two = Invite.make!(:accepted)
    invite_three = Invite.make!(:mailable)

    Invite.count.should eql(3)

    invite_one.accepted_pledge.pledge_topic.accepted_invites_count.should eql(1)
  end
end
