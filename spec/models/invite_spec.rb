require 'spec_helper'

describe Invite do
  describe 'invite_active?' do
    it 'should return false if no invite exists' do
      Invite.invite_active?(Devise.friendly_token).should be_false
    end

    it 'should return false if invite exists but the user has already been assigned.' do
      invite = Invite.make!(:accepted)

      Invite.invite_active?(invite.token).should be_false
    end

    it 'should return true if invite is created but a user has not accepted' do
      invite = Invite.make!(:mailable)

      Invite.invite_active?(invite.token).should be_true
    end
  end

  describe 'duplicate_email_validator' do
    it 'should pass with a nil email' do
      pledge = Pledge.make!

      invite = pledge.invites.build({token: Devise.friendly_token})

      invite.valid?.should be_true
    end

    it 'should pass with an empty email' do
      pledge = Pledge.make!

      invite = pledge.invites.build({token: Devise.friendly_token, email: ''})

      invite.valid?.should be_true
    end

    it 'should fail if duplicate emails exist for the same pledge' do
      pledge = Pledge.make!

      invite = pledge.invites.build({token: Devise.friendly_token, email: 'someguy@somewhere.com'})

      invite.save.should be_true

      invite = pledge.invites.build({token: Devise.friendly_token, email: 'someguy@somewhere.com'})

      invite.valid?.should be_false
    end
  end
end
