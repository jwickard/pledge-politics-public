require "spec_helper"

describe InviteMailer do
  it 'should send the user an invite email' do
    invite = Invite.make!(:mailable)
    mail = InviteMailer.invite_notification(invite)

    mail.subject.should eql("#{invite.inviting_pledge.user.email} Invites You To Pledge A Vote For #{invite.inviting_pledge.pledge_topic.name}")
    mail.to.should eq([invite.email])
    mail.from.should eq(["rp-mailer@pledgepolitics.com"])
    mail.body.encoded.should eq("#{invite.inviting_pledge.user.email} has invited you to pledge a vote for #{invite.inviting_pledge.pledge_topic.name}!\r\n\r\nFollow this link and you can pledge a vote for #{invite.inviting_pledge.pledge_topic.name} too!\r\n\r\n#{invite_show_url(token: invite.token)}\r\n\r\nTogether we can make a difference!\r\n")
  end
end
