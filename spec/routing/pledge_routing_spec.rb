require "spec_helper"

describe PledgeController do
  describe "routing" do
    it "routes to #new" do
      get("candidate/tomjeff/pledge/new").should route_to("pledge#new", :slug => "tomjeff")
    end

    it "routes to #show" do
      get("pledge/SKDFJJSKSKSKKSKF").should route_to("pledge#show", :token => "SKDFJJSKSKSKKSKF")
    end

    it "routes to #create" do
      get("candidate/tomjeff/pledge/create").should route_to("pledge#create", :slug => "tomjeff")
    end

    #  get "pledge/create"
    #
    #  get "pledge/edit"
    #
    #  get "pledge/update"
    #
    #  get "pledge/show"
    #
    #it "routes to #show with id" do
    #  get("/achievements/4").should route_to("achievements#show", :id => "4")
    #end

  end
end