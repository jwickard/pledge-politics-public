require "spec_helper"

describe OmniauthCallbacksController do
  describe "routing" do
    it "routes to #facebok" do
      get("users/auth/facebook/callback").should route_to("omniauth_callbacks#facebook")
    end

    it "routes to #twitter" do
      get("users/auth/twitter/callback").should route_to("omniauth_callbacks#twitter")
    end
  end
end