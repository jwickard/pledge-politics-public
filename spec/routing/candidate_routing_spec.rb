require "spec_helper"

describe PortalController do
  describe "routing" do
    it "routes to #show" do
      get("candidate/obama").should route_to("candidate#show", slug: 'obama')
    end
  end
end