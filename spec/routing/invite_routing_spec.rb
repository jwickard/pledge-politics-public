require "spec_helper"

describe InviteController do
  describe "routing" do
    it "routes to #create" do
      post("invite/create").should route_to("invite#create")
    end

    it "routes to #show" do
      token = Devise.friendly_token
      get("invite/#{token}").should route_to("invite#show", :token => token)
    end
  end
end