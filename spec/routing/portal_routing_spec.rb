require "spec_helper"

describe PortalController do
  describe "routing" do
    it "routes to #index" do
      get("portal/index").should route_to("portal#index")
    end

    it "root also routes to #index" do
      get("/").should route_to("portal#index")
    end
  end
end