# This file is copied to spec/ when you run 'rails generate rspec:install'
require 'simplecov'
SimpleCov.start 'rails'

ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'capybara/rspec'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  config.include JsonSpec::Helpers
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  config.extend ControllerMacros, :type => :controller
  config.include Devise::TestHelpers, :type => :controller
end

#setup oath testing
OmniAuth.config.test_mode = true
OmniAuth.config.mock_auth[:facebook] = {
   "provider"=>"facebook",
   "uid"=>"123456789",
   "credentials"=>{
       "token"=>"AAACqEn8SbTYBADxeKV3QLkGrzEsdfgsdfgsdfgz5teXbBFwO3Qh6HDd5vjZApMLmg2RaXmE3D8VaFRRtBHmwLHVInE9sSUZD",
       "expires_at" => "1334494673",
       "expires" => "true"
   },
   "info"=>{
       "nickname"=>"joe.user",
       "email"=>"user@example.com",
       "first_name"=>"Joe",
       "last_name"=>"Example",
       "name"=>"Joe Example",
       "image"=>"http://graph.facebook.com/775313787/picture?type=square",
       "urls"=>{
           "Facebook"=>"http://www.facebook.com/joel.wickard",
           "Website"=>nil
       }
   },
   "extra"=>{
       "raw_info"=>{
           "id"=>"123456789",
           "name"=>"Joe Example",
           "first_name"=>"Joe",
           "last_name"=>"Example",
           "link"=>"http://www.facebook.com/joel.wickard",
           "username"=>"joe.user",
           "gender"=>"male",
           "email"=>"user@example.com",
           "timezone"=>-6,
           "locale"=>"en_US",
           "verified"=>true,
           "updated_time"=>"2011-10-27T05:05:01+0000"
       }
   }
}

OmniAuth.config.mock_auth[:twitter] = {
  'provider' => 'twitter',
  'uid' => '123456789',
  'info' => {
    'nickname' => 'juser',
    'name' => 'Joe User',
    'location' => '',
    'image' => 'http://a1.twimg.com/profile_images/3737382/some_image.jpg',
    'description' => 'hugonaut',
    'urls' => {
      'Website' => 'http://www.joeuser.com',
      'Twitter' => 'http://twitter.com/juser'
    }
  },
  'credentials' => {
    'token' => '24228027-asdfadfadfadfadsfadsf9cUo0FzZT',
    'secret' => 'BSjhlKPwVxQ66ETOmAdomjzKsyNZ6vTKasdfasdfeRQ'
  },
  'extra' => {
    'raw_info' => {
      'id' => '123456789',
      'name' => 'Joe User',
      'verified' => false
    }
  }
}

OmniAuth.config.mock_auth[:google] = {
  'provider' => 'google',
  'uid' => 'https://www.google.com/accounts/o8/id?id=AItOawlpqP92_h9-DL4WUSUGaRr7nwoYz8oLf3U',
  'info' => {
    'email' => 'joegmail@gmail.com',
    'first_name' => 'Joe',
    'last_name' => 'Gmail',
    'name' => 'Joe Gmail'
  }
}