require 'uri'

module ApplicationHelper
  def tweet_text(candidate)
    URI.escape("Check Out My Pledge To Vote For #{candidate.name}:", Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))
  end

  def is_referral?
    (!session['pledge.pledge_token'].nil? && !session['pledge.pledge_token'].empty?) or (!session['pledge.invite_token'].nil? && !session['pledge.invite_token'].empty?)
  end

  def active_candidates_by_name
    Candidate.active_candidates_by_name
  end
end
