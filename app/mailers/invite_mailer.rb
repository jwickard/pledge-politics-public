class InviteMailer < ActionMailer::Base
  default from: "rp-mailer@pledgepolitics.com"

  def invite_notification(invite)
    @username = invite.inviting_pledge.user.email
    @invite = invite
    mail(to: invite.email, subject: "#{@username} Invites You To Pledge A Vote For #{invite.inviting_pledge.pledge_topic.name}")
  end
end
