$.initializePledge = function(token, pledge_id){
    $('#pledge-direct-link').val('http://www.pledgepolitics.com/pledge/'+token);

    //twitter button
    $('#twitter-button').attr('data-url', 'http://www.pledgepolitics.com/pledge/'+token);

      !function(d,s,id){
          var js,fjs=d.getElementsByTagName(s)[0];
          if(!d.getElementById(id)){
              js=d.createElement(s);
              js.id=id;
              js.src="//platform.twitter.com/widgets.js";
              fjs.parentNode.insertBefore(js,fjs);
          }
      }(document,"script","twitter-wjs");

    //facebook like button stuff
    $('.fb-send').attr('data-href', 'http://www.pledgepolitics.com/pledge/'+token);
    $('.fb-like').attr('data-href', 'http://www.pledgepolitics.com/pledge/'+token);

    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=192527500854208";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    //google +
    $('#google-plus-button').attr('data-href', 'http://www.pledgepolitics.com/pledge/'+token);
    gapi.plusone.go();

    $('#like-actions').slideDown();

    //update the id for the pledge invite form
    $('#invite_invite_pledge_id').val(pledge_id);
    //enable content.
    $('#pledge-actions').removeClass('disabled-section');
    $('#invite_email').removeAttr('disabled');
    $('#invite_submit').removeAttr('disabled');
    $('#pledge-direct-link').removeAttr('disabled');
    $('#pledge-direct-link-button').removeAttr('disabled');
    //flash message.
    $.fn.flashSuccess('Created Pledge!');
};

$(document).ready(function(){

    ZeroClipboard.setMoviePath('/assets/ZeroClipboard.swf');

    var clip = new ZeroClipboard.Client();
    clip.setHandCursor(true);

    clip.addEventListener('complete', function (client, text) {
        //we dont' care if they copy while disabled, however make it look like it's disabled.
        if(!$('#pledge-actions').hasClass('disabled-section')){
            $.fn.flashSuccess("Copied link to clipboard: " + text );
        }
    });

    clip.addEventListener('mouseDown', function(client){
        clip.setText($('#pledge-direct-link').val());
    });

    clip.glue( 'd_clip_button', 'd_clip_container' );

    $('#pledge-button').click(function(e){
        //do not follow our link
        e.preventDefault();

      $('#pledge-spinner').show();

      $.getJSON($(this).attr('href'), function(data){
        $.initializePledge(data['token'], data['id']);
        $('#pledge-spinner').hide();
      }).error(function(errors){
        //TODO figure out what's up with the error parsing double 'doody'
        var resp = $.parseJSON(errors['responseText']);
        $('#pledge-spinner').hide();
        $.fn.flashError(resp["user_id"][0]);
        $('#invite-block').addClass('highlight', 12000);
        $('#pledge-block').removeClass('highlight');
      });
    });

    $('#new_invite').bind('ajax:success', function(data, status, xhr){
        $.fn.flashSuccess('Invite was successfully sent!');
    })
    .bind('ajax:error', function(xhr, status, error){
        resp = $.parseJSON(status['responseText']);
        $.fn.flashError(resp["email"][0]);
    })


});
