$ = jQuery

$.fn.flashError = (msg) ->
  $('#error-holder').text(msg)
  $('#error-holder')
  .show()
  .offset({top: $(document).scrollTop(), 0})
  .delay(3000)
  .fadeOut('slow')

$.fn.flashSuccess = (msg) ->
  $('#success-holder').text(msg)
  $('#success-holder')
  .show()
  .offset({top: $(document).scrollTop(), 0})
  .delay(3000)
  .fadeOut('slow')