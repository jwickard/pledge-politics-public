class User < ActiveRecord::Base
  has_many :authentications
  has_one :pledge
  has_one :invite

  validates_presence_of :user_name, :provider

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :user_name, :provider, :email, :password, :password_confirmation, :remember_me

  def self.find_for_facebook_oauth(oauth, signed_in_user=nil)

    if signed_in_user

      if auth = signed_in_user.authentications.find_by_uid_and_provider(oauth['uid'], oauth['provider'])
        auth.update_attributes(token: oauth['credentials']['token'], token_expires: oauth['credentials']['expires'], token_duration: oauth['credentials']['expires_at'])
      else
        signed_in_user.authentications.create!(uid: oauth['uid'], provider: oauth['provider'], token: oauth['credentials']['token'])
      end

      return signed_in_user
    else
      #try to find user by authentication
      user = User.joins('LEFT OUTER JOIN authentications ON authentications.user_id = users.id').where("email = :email or (authentications.provider = :provider and authentications.uid = :uid)", {email: oauth['extra']['raw_info']['email'], provider: oauth['provider'], uid: oauth['uid']}).readonly(false).first

      #if we weren't able to look a user up, create one:
      if user.nil?
        user = User.new(user_name: oauth['info']['nickname'], email: oauth['info']['email'], provider: 'facebook', password: Devise.friendly_token[0,20])
      end

      #update or build authentication
      if auth = user.authentications.find_by_uid_and_provider(oauth['uid'], oauth['provider'])
        auth.update_attributes(token: oauth['credentials']['token'])
      else
        user.authentications.build(uid: oauth['uid'], provider: oauth['provider'], token: oauth['credentials']['token'], token_expires: oauth['credentials']['expires'], token_duration: oauth['credentials']['expires_at'])
      end

      user.save!

      return user
    end

  end

  def self.find_for_twitter_oauth(oauth, signed_in_user=nil)

    if signed_in_user
      if auth = signed_in_user.authentications.find_by_uid_and_provider(oauth['uid'], oauth['provider'])
        auth.update_attributes(token: oauth['credentials']['token'], secret: oauth['credentials']['secret'])
      else
        signed_in_user.authentications.create!(uid: oauth['uid'], provider: oauth['provider'], token: oauth['credentials']['token'], secret: oauth['credentials']['secret'])
      end

      return signed_in_user
    else
      #twitter find by nickname provider or authentication
      user = User.joins('LEFT OUTER JOIN authentications ON authentications.user_id = users.id').where("(users.user_name = :user_name and users.provider = 'twitter') or (authentications.provider = :provider and authentications.uid = :uid)", { user_name: oauth['info']['nickname'], provider: oauth['provider'], uid: oauth['uid'] }).readonly(false).first

      #if we weren't able to look a user up, create one:
      if user.nil?
        user = User.new(user_name: oauth['info']['nickname'], email: oauth['info']['email'], provider: 'twitter', password: Devise.friendly_token[0,20])
      end

      #update or build authentication
      if auth = user.authentications.find_by_uid_and_provider(oauth['uid'], oauth['provider'])
        auth.update_attributes(token: oauth['credentials']['token'], secret: oauth['credentials']['secret'])
      else
        user.authentications.build(uid: oauth['uid'], provider: oauth['provider'], token: oauth['credentials']['token'], secret: oauth['credentials']['secret'])
      end

      user.save!

      return user
    end
  end

  def self.find_for_open_id_oauth(oauth, signed_in_user=nil)

    if signed_in_user
      auth = signed_in_user.authentications.find_by_uid_and_provider(oauth['uid'], oauth['provider'])

      if auth.nil?
        signed_in_user.authentications.create!(uid: oauth['uid'], provider: oauth['provider'])
      end

      return signed_in_user
    else
      user = User.joins('LEFT OUTER JOIN authentications ON authentications.user_id = users.id').where("(authentications.provider = :provider and authentications.uid = :uid) or (users.email = :email)", { provider: oauth['provider'], uid: oauth['uid'], email: oauth['info']['email'] } ).readonly(false).first

      if user.nil?
        user = User.new(user_name: oauth['info']['email'], email: oauth['info']['email'], provider: 'google', password: Devise.friendly_token[0,20])
      end

      auth = user.authentications.find_by_uid_and_provider(oauth['uid'], oauth['provider'])

      if auth.nil?
        #build a new authentication
        user.authentications.build(uid: oauth['uid'], provider: oauth['provider'])
      end

      user.save!

      return user
    end
  end

  #devisey place to apply our own initialization
  def self.new_with_session(params, session)
    super.tap do |user|
      #we are running through the registrations controller so I want to assume we are only handling pledgepolitics users here.
      user.provider = 'pledgeforpaul'
      user.user_name = user.email
    end
  end

  def email_required?
    false
  end
end
