class Invite < ActiveRecord::Base

  belongs_to :inviting_pledge, foreign_key: :invite_pledge_id, class_name: 'Pledge'
  has_one :accepted_pledge, foreign_key: :accepted_invite_id, class_name: 'Pledge'
  belongs_to :user

  validates_presence_of :invite_pledge_id
  validate :duplicate_email_validator

  def self.invite_active?(token)
    Invite.where("token = :token AND user_id IS NULL", { token: token }).exists?
  end

  def duplicate_email_validator
    if !email.nil? and !email.empty? and !persisted?
      count = Invite.where("email = :email and invite_pledge_id = :id", {email: email, id: invite_pledge_id}).count

        if count > 0
          errors.add(:email, "You have already sent an invite to: #{email}")
        end
    end
  end
end
