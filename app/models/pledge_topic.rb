class PledgeTopic < ActiveRecord::Base
  has_many :pledges

  has_attached_file :profile_image, styles: { medium: "150x200>", thumb: "50x50>" }

  def pledge_count
    #Force reload of our pledge collection.
    pledges(true).size
  end

  def accepted_invites_count
    Invite.joins("LEFT OUTER JOIN pledges ON pledges.accepted_invite_id = invites.id").where("pledges.pledge_topic_id = :pledge_topic_id AND accepted = true", { pledge_topic_id: self.id }).count()
  end
end
