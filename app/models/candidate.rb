class Candidate < PledgeTopic
  def self.active_candidates_by_name
    Candidate.find_all_by_active(true)
  end
end
