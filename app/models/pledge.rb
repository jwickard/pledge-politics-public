class Pledge < ActiveRecord::Base
  belongs_to :user
  belongs_to :pledge_topic
  has_many :invites, foreign_key: 'invite_pledge_id'
  belongs_to :accepted_invite, foreign_key: :accepted_invite_id, class_name: 'Invite'

  validates_uniqueness_of :user_id, message: 'You have already pledged with this account!'
end