ActiveAdmin.register Candidate do

  index do
    column :profile_image do |candidate|
      image_tag candidate.profile_image.url(:medium)
    end

    column :name
    column :slug
    column :hash_tag
    column :active
    column :created_at
    column :updated_at

    default_actions
  end

  form html: { enctype: "multipart/form-data" } do |f|
    f.inputs do
      f.input :name
      f.input :slug
      f.input :hash_tag
      f.input :profile_image, label: "Attach Profile Image", as: :file
      f.input :active
    end

    f.buttons
  end

  show do |candidate|
    attributes_table do
      row :profile_image do
        image_tag candidate.profile_image.url(:medium)
      end
      row :name
      row :slug
      row :hash_tag
      row :active
    end
  end
end
