class InviteController < ApplicationController
  before_filter :authenticate_user!, :except => :show

  def create
    posted_mail = params[:invite][:email]

    #validate email
    if posted_mail.nil? or posted_mail.empty?
      #TODO maybe we push the validation through the model somehow?
      render json: {email: ["can't be blank"]}, status: :unprocessable_entity and return
    end

    invites = posted_mail.split(',')

    current_invite = nil

    parsed_invites = []
    #TODO rewrite this as a built up transaction
    Invite.transaction do
      begin
        invites.each do |mail|
          if !mail.nil? && !mail.empty?
            current_invite = Invite.new(invite_pledge_id: params[:invite][:invite_pledge_id], email: mail, token: Devise.friendly_token)

            current_invite.save!

            parsed_invites << current_invite
          end
        end
      rescue
        #failed, tell the user!
        render json: current_invite.errors, status: :unprocessable_entity and return
      end
    end

    #we parsed all of the invites, so let's send out email.
    #TODO find out if a try/catch will rollback action mailer.
    #TODO 2 we are going to delayed_job these, when that happens, we should be able to include that in the database rollback. try it!
    parsed_invites.each do |invite|
      InviteMailer.invite_notification(invite).deliver()
    end

    #we consider this successful
    render json: parsed_invites
  end

  def show

    invite = Invite.where("token = :token AND user_id IS NULL", { token: params['token'] }).first

    if invite
      session['pledge.invite_token'] = params[:token]
      redirect_to view_candidate_path(invite.inviting_pledge.pledge_topic.slug)
    else
      logger.error("Someone followed an invite token into the site that didn't exist: #{params[:token]}")
      redirect_to root_path
    end
  end
end
