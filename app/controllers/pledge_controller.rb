class PledgeController < ApplicationController
  before_filter :authenticate_user!, :except => :show

  def new
    @candidate = current_candidate
  end

  def create
    @pledge = nil
    target = nil

    #check if we have a related pledge
    if !session[PLEDGE_TOKEN_KEY].nil? and !session[PLEDGE_TOKEN_KEY].empty?
      logger.info("found related pledge token: #{session[PLEDGE_TOKEN_KEY]} when creating pledge for: #{current_user.id}")
      #we are going to create a new pledge and a JIT invite object for this new pledge.
      invite_pledge = Pledge.find_by_token(session[PLEDGE_TOKEN_KEY])

      invite = invite_pledge.invites.build({ accepted: true, user: current_user, token: Devise.friendly_token })
      @pledge = invite.build_accepted_pledge({ user: current_user, token: Devise.friendly_token, pledge_topic: invite_pledge.pledge_topic })
      target = invite
    elsif !session[INVITE_TOKEN_KEY].nil? and !session[INVITE_TOKEN_KEY].empty?
      logger.info("found related invite token: #{session[INVITE_TOKEN_KEY]} when creating pledge for: #{current_user.id}")
      #we will just associate the new pledge with the existing invite.
      invite = Invite.find_by_token(session[INVITE_TOKEN_KEY])
      invite.accepted = true
      invite.user = current_user
      @pledge = invite.build_accepted_pledge({ user: current_user, token: Devise.friendly_token, pledge_topic: invite.inviting_pledge.pledge_topic })
      target = invite
    else
      candidate = current_candidate
      @pledge = Pledge.new(user: current_user, token: Devise.friendly_token, pledge_topic: candidate)
      target = @pledge
    end

    @candidate = @pledge.pledge_topic

    respond_to do |format|
      if target.save
        format.html { render action: :new }
        format.json { render json: @pledge }
      else
        format.html { redirect_to view_candidate_path(@candidate.slug), alert: target.errors.full_messages[0] }
        format.json { render json: @pledge.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    #a user has followed a pledge back to the site.

    #we either 1.) create an invite record now and track them until they sign up.
    #2.) track them sessionally and create an invite record for status upon sign up.

    pledge = Pledge.where(token: params[:token]).first

    if pledge
      session['pledge.pledge_token'] = pledge.token
      redirect_to view_candidate_path(pledge.pledge_topic.slug)
    else
      logger.error("Someone followed a pledge token into the site that didn't exist: #{params[:token]}")
      redirect_to root_path
    end
  end

  private

  def current_candidate
    Candidate.where("active = ? and slug = ?", true, params[:slug]).first
  end
end
