class PortalController < ApplicationController
  def index
    @candidates = Candidate.find_all_by_active(true)
  end

  def sponsor
  end

  def contact
  end
end
