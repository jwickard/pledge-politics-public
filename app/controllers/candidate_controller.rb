class CandidateController < ApplicationController
  def show
    @candidate = Candidate.find_by_slug(params[:slug])

    if @candidate.nil? or not @candidate.active?
      redirect_to root_path
    else
      #store candidate id in session
      session["user_return_to"] = pledge_new_path(@candidate.slug)

      #TODO these need to be within the context of a candidate
      @total_pledge_count = @candidate.pledge_count
      @total_accepted = @candidate.accepted_invites_count
    end
  end
end
