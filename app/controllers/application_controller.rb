class ApplicationController < ActionController::Base
  protect_from_forgery

  INVITE_TOKEN_KEY = 'pledge.invite_token'
  PLEDGE_TOKEN_KEY = 'pledge.pledge_token'

  def after_sign_in_path_for(resource)
    stored_location_for(resource) || root_path
  end
end
