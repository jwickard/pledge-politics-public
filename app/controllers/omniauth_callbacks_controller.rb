class OmniauthCallbacksController < Devise::OmniauthCallbacksController

  #TODO figure out if we have any / what kind of verification we need to do in absence of this filter.
  #without this filter enabled, our session resets whenever we use google.
  protect_from_forgery :except => :google

  def facebook
    @user = User.find_for_facebook_oauth(request.env['omniauth.auth'], current_user)

    log_user_in(@user, 'Facebook')
  end

  def twitter
    @user = User.find_for_twitter_oauth(request.env['omniauth.auth'], current_user)

    log_user_in(@user, 'Twitter')
  end

  def google
    @user = User.find_for_open_id_oauth(request.env['omniauth.auth'], current_user)

    log_user_in(@user, 'Google')
  end

  private

  def log_user_in(user, provider)
    if !user.nil? and user.persisted?
      flash[:notice] = I18n.t 'devise.omniauth_callbacks.success', kind: provider
      sign_in_and_redirect user, event: :authentication
    end
  end
end