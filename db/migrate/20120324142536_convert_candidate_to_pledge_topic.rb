class ConvertCandidateToPledgeTopic < ActiveRecord::Migration
  def up
    rename_table :candidates, :pledge_topics
    add_column :pledge_topics, :type, :string
    rename_column :pledges, :candidate_id, :pledge_topic_id
    PledgeTopic.update_all(type: "Candidate")
  end

  def down
    remove_column :pledge_topics, :type
    rename_table :pledge_topics, :candidates
    rename_column :pledges, :pledge_topic_id, :candidate_id
  end
end
