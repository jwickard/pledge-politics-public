class AddUserNameAndProviderToUser < ActiveRecord::Migration
  def change
    add_column :users, :user_name, :string, :null => false
    add_column :users, :provider, :string, :null => false

    add_index :users, [:user_name, :provider], :unique => true
  end
end
