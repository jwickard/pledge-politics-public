class AddProfileImageToCandidate < ActiveRecord::Migration
  def self.up
    change_table :candidates do |t|
      t.has_attached_file :profile_image
    end
  end

  def self.down
    drop_attached_file :candidates, :profile_image
  end
end
