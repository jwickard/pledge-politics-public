class AddRelatedPledgesToInvite < ActiveRecord::Migration
  def change
    rename_column :invites, :pledge_id, :invite_pledge_id
    add_column :pledges, :accepted_invite_id, :integer
  end
end
