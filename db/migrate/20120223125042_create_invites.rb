class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites do |t|
      t.integer :user_id
      t.integer :pledge_id
      t.string :token
      t.string :email
      t.string :uid
      t.boolean :accepted

      t.timestamps
    end
  end
end
