require 'bundler/capistrano'

set :rvm_ruby_string, '1.9.3-p194'
require "rvm/capistrano"

load 'deploy/assets'

set :user, 'jwickard'
set :application, "pledgeforpaul"
set :repository,  "git@joelwickard.unfuddle.com:joelwickard/pfp.git"

set :scm, 'git'
set :use_sudo, false
set :deploy_to, "/var/vhosts/pledgeforpaul"

role :app, "carla"
role :web, "carla"
role :db,  "carla", :primary => true

set :unicorn_binary, "/home/jwickard/.rvm/gems/ruby-1.9.3-p194/bin/unicorn_rails"
set :unicorn_config, "#{current_path}/config/unicorn.rb"
set :unicorn_pid, "/var/vhosts/pledgeforpaul/shared/pids/unicorn.pid"

namespace :deploy do
  task :start, :roles => :app, :except => { :no_release => true } do
    run "cd #{current_path} && #{try_sudo} #{unicorn_binary} -c #{unicorn_config} -E production -D"
  end
  task :stop, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} kill `cat #{unicorn_pid}`"
  end
  task :graceful_stop, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} kill -s QUIT `cat #{unicorn_pid}`"
  end
  task :reload, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} kill -s USR2 `cat #{unicorn_pid}`"
  end
  task :restart, :roles => :app, :except => { :no_release => true } do
    stop
    start
  end
end

set :branch do
  default_tag = `git tag`.split("\n").last

  tag = Capistrano::CLI.ui.ask "Tag to deploy (make sure to push the tag first): [#{default_tag}] "
  tag = default_tag if tag.empty?
  tag
end