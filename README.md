# Pledge Politics #

Rails application for allowing users to publicly endorse a political candidate and invite friends through their social networks to also endorse the candidate.

### Tools ###

* Rails / Omniauth / Oauth2 / Bootstrap
* Continuous Testing with guard / Rspec / Machinist
* Automated deployments with Capistrano / Rackspace
* Automated error reporting with Airbrake.io / New Relic
 
